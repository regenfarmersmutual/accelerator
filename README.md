### Documentation template

You can use this MKDocs template to create a static HTML webpage of whatever documentation you save in this repository.  It creates a nice indexed structure like this https://www.mkdocs.org/user-guide/writing-your-docs/.

### Using this template

To use this template, you need to:

1. Change these two lines in mkdocs.yml file in the main folder
   - site_name: Regen Accelerator <-- add your site name here
   - site_url: https://regenfarmersmutual.gitlab.io/accelerator/ <-- change this to your group or subgroup name

Now you can add markdown files to your repository and the index will match the folder structure of your files!

### Learn more about mkdocs generally 

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.
