# Week 1 - Setting the Scene

### Session objectives
Understanding how environmental markets can deliver landscape scale outcomes.

**Output:** Each group will define its team roles and resourcing requirements and add this to their template transaction documentation. 

### Format
An online video conferencing session with the whole cohort. 

**Presentation:** The Program Facilitators will introduce participants to the course, including

- Understanding program objectives, process and guiding principles 
- Introduction to group structure, team roles and resources 
- Introduction to template documentation (pitch, term sheet, farm plan and engagement plan)

**Group Activity:** Allocating roles among team members and agreeing how and when your group will communicate over the course of the accelerator. Explore supporting resources. You will be allocated a budget - start thinking about the resources you need and what resources you currently have access to.

### Attending
Landscape Impact Groups, mentors, program facilitators

### Resources

- <a href="https://drive.google.com/drive/folders/1qt15dUOftaWnZUxWnaod_0vknqk0cTrw?usp=sharing">Project Pitches</a>
- <a href="https://drive.google.com/drive/folders/1PPQgXkIWKywSfIuyjdtmiY81-jyhb05Z?usp=sharing">Term Sheets</a>
- <a href="https://drive.google.com/drive/folders/1T1At2F8anqVqeU3P5YWoYpquPgMqw7xF?usp=sharing">Engagement Plans</a>

