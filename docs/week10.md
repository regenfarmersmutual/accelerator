# Week 10 - Structuring a transaction 3

### Session objectives
Defining the terms and conditions of a transaction

**Output:**  Definition of key transaction terms with prospective buyers 

### Format
RFM arranges and facilitates online negotiations between each group to prospective buyers.

**Session outline:**
Iterative development of the landscape scale impact transaction with potential buyers

- Walk-through documentation terms
- Iteratively test assumptions with key parties
- Negotiate and agree upon indicative transaction terms and conditions

**Activity:**
Group - engage with any cornerstone buyers and identify key variables that underpin the impacts and economics of the transaction. Refine and record video of pitch for presenting to farmers, buyers and market more generally. 

### Attending
Landscape Impact Groups, mentors, sponsors, program facilitators

### Resources
Links…