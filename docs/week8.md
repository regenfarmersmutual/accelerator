# Week 8 - Structuring a transaction 1

### Session objectives
Testing buyer appetite for a potential transaction

**Output:**  Refined pitch deck with indicative buyer interest 

### Format
RFM arranges and facilitates online presentations by each group to prospective buyers.

**Session outline:**
Iterative development of the transaction with potential buyers

- Presenting ‘pitch’ to local and national buyers
- Understanding potential demand and constraints
- Refining transaction parameters and identifying key variables

**Activity:**
Group - Refine your group’s pitch deck based on the feedback from your prospective buyer pitch presentations with the assistance of your mentor.

### Attending
Landscape Impact Groups, mentors, sponsors, buyers, program facilitators

### Resources
Links…