# Week 6 - Community of Practice 2

### Session objectives
Sharing and learning session with cohort 

**Output:**  Collating feedback and best practice from others to refine pitch deck 

### Format
An online conferencing session with the whole cohort - breakout rooms into your own groups.

**Session outline:**
Drawing on the wisdom of the crowd

- Each group presents 5-minute transaction ‘pitch’ 
- Feedback and analysis from expert panel
- Workshop themes and best practice by role

**Activity:**
Group - in breakout rooms, each role will discuss themes and best practice that emerged from the presentations. 

### Attending
Landscape Impact Groups, mentors, sponsors, program facilitators

### Resources
Links…

