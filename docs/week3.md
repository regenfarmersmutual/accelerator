# Week 3 - Scoping the opportunity - part 1

### Session objectives
A tour of environmental markets and their relevance to farmers

**Output:**  An understanding of current and emerging market opportunities. 

### Format
An online conferencing session with the whole cohort - with presentations by mentors & market experts.

**Session outline:** Introduction to environmental markets and assets

- Carbon, biodiversity, hydrology 
- Whole-of-farm, soil health, nutrition
- Green provenance, property values, industry certification

**Activity:** Case study using one of the proposed transactions. Then review your prospective transaction - are there additional markets your group could align with for delivering landscape scale impacts? How does this impact your transaction or engagement strategy? Who would you need to engage to find out more?

### Attending
Landscape Impact Groups, mentors & market experts, program facilitators

### Resources
Links…

