# Week 11 - Community of Practice 3

### Session objectives
Refining prospective transactions with cohort

**Output:**  Prospective transaction is documented and ready for presenting. 

### Format
Each group works with their mentor to finalise their documentation and video.

**Session outline:**
Group’s share via the Accelerator drive their

- Farmer engagement strategy
- Indicative term sheet
- Transaction budget and financial model
- Pitch deck

**Activity:**
Each group works at their own pace to refine and finalise their documentation and complete their video.

### Attending
Landscape Impact Groups, mentors, sponsors, program facilitators

### Resources
Links…