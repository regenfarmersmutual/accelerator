![Regen Farmers Mutual Landscape Impact Accelerator](images/rfmlogo.png)

# Welcome to the RFM Accelerator Program

### Program Description
The RFM Landscape Impact Accelerator Program aims to help groups of farmers to scope, value, structure and ultimately execute environmental market transactions that can deliver landscape scale impacts across Australia. The program takes twelve Landscape Impact Groups (60 participants), through a 12-week program where they co-design prospective transactions with market experts, corporate buyers, government, Landcare networks and farmer organisations. 

![Overview of the RFM Landscape Impact Accelerator](images/acceleratorChevron.png)

### Program Outcomes
By the end of the Program, which is scheduled to run in Q2 2022, groups can expect to have co-designed:

- a potential transaction with input from industry experts and prospective buyers; and, 
- have an implementation plan for rolling out the transaction in their region or industry.

In addition to helping the participating Landscape Impact Groups to structure and implement landscape scale environmental transactions, the facilitated co-design process will assist in: 

- Educating leaders and creating a Community of Practice
- Helping prospective buyers inform and shape environmental markets 
- Creating diverse examples of landscape scale transactions and farmer engagement processes across different regions, industries, and environmental markets

Furthermore, the program will create opportunities for employment and secure income streams for landscape agronomists and advisors who are needed to assist in the ongoing management and delivery of transactions, and the development of further transactions across Australia.


### Landscape Impact Group requirements
The Landscape Impact Accelerator will invite farmer and land manager groups into the Program that have identified a prospective landscape scale opportunity that has potential value to at least one of the Program sponsors. The Landscape Impact group must submit an application form providing prescribed details with respect to that transaction.

Additionally, each group needs:

- At least five members - who will fulfil defined roles within the team (organiser, financial analyst, landscape agronomist, marketer/community manager, and ‘farmer’ avatar)
- Capacity to participate for up to 4 hours each week for twelve weeks
- A nominated organiser 


### Accelerator Outline


|         Date | Session Title                                                                                             |
|-------------:|-----------------------------------------------------------------------------------------------------------|
| Week #1<br>  | Setting the Scene:<br>Using environmental markets to deliver landscape scale outcomes.                    |
|      Week #2 | Creating a community of practice:<br>Meet and greet with sponsors, mentors and other participants         |
|      Week #3 | Understanding the opportunity - part 1:<br>A tour of environmental markets and their value to farmers     |
|      Week #4 | Understanding the opportunity - part 2:<br>Mapping a farm’s environmental assets                          |
|      Week #5 | Understanding the opportunity - part 3:<br>Scoping landscape impact of potential transactions             |
|      Week #6 | Community of practice - part 2:<br>Sharing and learning session with cohort                                        |
|      Week #7 | Engaging farmers:<br>Testing farmer appetite for a potential transaction                |
|      Week #8 | Structuring a landscape scale transaction - part 1:<br>Testing buyer appetite for a potential transaction |
|      Week #9 | Structuring a landscape scale transaction - part 2:<br>Building a financial model of the transaction      |
|     Week #10 | Structuring a landscape scale transaction - part 3:<br>Defining the terms and conditions of a transaction |
|     Week #11 | Community of practice - part 3:<br>Refining proposed transactions with cohort                                      |
|     Week #12 | Accelerator Pitch Night:<br>Each group presents their transaction in an open-invite event               |





