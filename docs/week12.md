# Week 12 - Accelerator Pitch Night 

### Session objectives
Each group presents their transaction in an open-invite event to introduce the landscape impact transactions to the public and pitch to a live audience including buyers, farmers and other stakeholders.

**Output:**  Landscape impact transactions are ready to engage with farmers and buyers.  

### Format
Each group presents online in a public event - with questions and feedback from a wide audience.

**Session outline:**
Group’s present their transaction

- Transaction pitch video
- Q&A from participants and expert panel
- Introduce their profile and transaction page

**Activity:**
Facilitated feedback session with session attendees. 

### Attending
Landscape Impact Groups, mentors, sponsors, program facilitators

### Resources
Links…