# Week 7 - Engaging farmers 1

### Session objectives
Testing farmer appetite for a potential transaction 

**Output:**  Refined pitch deck including terms for indicative farmer support 

### Format
Each group holds a farmer event or undertakes kitchen table meetings.

**Session outline:**
Introducing transaction to farmers

- Presenting pitch to farmers
- Gathering and analysing feedback
- Refining transaction parameters

**Activity:**
Each group conducts kitchen table talks or events with farmers in your group’s network. Collect 10 feedback forms from farmers - RFM to provide aggregated feedback to cohort. Refine your group’s pitch deck based on these discussions with your mentor.  

### Attending
Landscape Impact Groups, mentors, program facilitators

### Resources
Links…

