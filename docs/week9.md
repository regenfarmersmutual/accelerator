# Week 9 - Structuring a transaction 2

### Session objectives
Learning by comparing financial models of prospective transactions

**Output:**  Optimising economics of the transaction for the farmer, the group and the buyer. 

### Format
An online conferencing session with the whole cohort - breakout rooms into your own groups.

**Session outline:**
Modelling the proposed transaction to understand its economics

- Dissecting financial models and key risks
- Iteratively testing assumptions with key parties
- Understanding ROI and key variables

**Activity:**
The finance role of each group will present the financial model of their transaction (including key variables, risks, parties) and take comments/questions from the cohort.

### Attending
Landscape Impact Groups, mentors, sponsors, program facilitators

### Resources
Links…