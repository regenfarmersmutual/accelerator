# Week 5 - Scoping the opportunity - part 2

### Session objectives
Scoping landscape impact of prospective transactions

**Output:** Definition of the landscape scale opportunity 

### Format
An video conferencing session with the whole cohort - breakout rooms into your own groups.

**Session outline:** Documenting your prospective transaction

- Building from existing resources (NRM plans etc)
- Clear value statement for potential transaction
- Broad definition of actions required to deliver impact
- Identifying impact measurement methods
- Updating your ‘pitch’ document

**Activity:** Beginning to refine your pitch

- Group - In your group, use existing plans to develop a clear value statement for your transaction. Define the actions required to deliver landscape impact. What methods will your group use? With advice from your mentor, develop your Landscape Impact Pitch.
- Individual - Reflect on your role and how it impacts on your slide in the pitch deck

### Attending
Landscape Impact Groups, mentors, program facilitators

### Resources
Links…
