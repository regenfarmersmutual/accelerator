# Week 4 - Scoping the opportunity - part 2

### Session objectives
Mapping a farm’s environmental assets 

**Output:**  Identifying the tools to measure landscape impact 

### Format
An online conferencing session with the whole cohort - breakout rooms into your own groups.

**Session outline:**
Introduction to approaches and tools for measuring and analysing opportunities:

- Introduction to Environmental Farm Assessment
- Understanding landscape agronomy and role of landscape agronomist
- From indicative opportunity to farm plan to stewardship agreement

**Activity:**
Build a digital twin live with one of the participants using an Environmental Farm Assessment. Identify/brainstorm the environmental assets of your group? What data have you got already, what data do you think you need at the farm scale. 

### Attending
Landscape Impact Groups, mentors, program facilitators

### Resources
Links…Google drive, Onboarding wiki (gitlab), Farm Digital Twin
