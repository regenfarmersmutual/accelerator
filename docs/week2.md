# Week 2 - Creating a community of practice

### Session objectives
Meet and greet with other participants, mentors and sponsors.

**Output:**  Community of Practice established and understanding of individual roles

### Format
An online video conferencing session with the whole cohort. 

**Session outline:** Laying foundations for community of practice

- Introducing sponsors and mentors
- Cohort networking via zoom
- Definition of each individual’s role

**Activity:**
1) Group - Introduce your group to the mentor panel. Share your initial ideas for a landscape ímpact transaction, review resource requirements from the previous week. Discuss your network and who you can engage to scale up your transaction.
2) Individual - Breakout rooms by role to cross-pollinate networks. Create a profile in the LInkedIn Community of Practice and post something about what you hope to get from the program.

### Attending
Landscape Impact Groups, mentors, program facilitators

### Resources
<a href="https://www.linkedin.com/groups/14052300/">Community of Practice on LinkedIn</a>